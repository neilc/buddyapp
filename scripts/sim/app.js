'use strict'

const https = require('https')
const http = require('http')
const URI = require('url')
const path = require('path')
const WebSocket = require('ws')

// -----------------------------------------------------------------------------------------------------------------------------------------

const IDENTITY_SERVICE_URL = "https://layer-identity-provider.herokuapp.com"
const LAYER_APP_ID = "layer:///apps/staging/5aba1cd2-c195-11e4-92b9-4e88000000f4"
const LAYER_CONVERSATION = "layer:///conversations/96376619-05d4-407e-87d5-747cf10f7545"
const TEST_USER_FIRST_NAME = "Neil"
const TEST_USER_LAST_NAME = "Cowburn"

// -----------------------------------------------------------------------------------------------------------------------------------------

class LayerClient {
  constructor (appID) {
    this.appID = appID
    this.sessionToken = ''
  }
  
  authenticate (firstName, lastName, callback) {
    this._nonce((nonceResult) => {
      if (nonceResult == null) {
        console.error("Something went wrong requesting the nonce")
        return 
      }
      
      const result = JSON.parse(nonceResult)
      
      this._identityToken(firstName, lastName, result.nonce, (tokenResult) => {
        if (tokenResult == null) {
          console.error("Something went wrong requesting the identity token")
          return 
        }
        
        const result = JSON.parse(tokenResult)
        
        this._sessionToken(result.identity_token, (sessionResult) => {
          if (sessionResult == null) {
            console.error("Something went wrong requesting the session token")
            return 
          }
          this.sessionToken = sessionResult.session_token
          callback(this.sessionToken)
        })
      })
    })
  }
  
  _newRequest (method, url, headers, callback) {
    headers = headers || {}
    const uri = URI.parse(url)
    const useSSL = uri.protocol == "https:"
    const port = uri.port || (uri.protocol == "http:" ? 80 : 443)
    const host = uri.host
    const path = uri.path
    
    var options = {
      method: method, 
      host: host, 
      port: port, 
      path: path, 
    }
    
    if (headers != {}) {
      options["headers"] = headers
    }
    
    return useSSL ? https.request(options, callback) : http.request(options, callback)
  }
  
  _nonce (callback) {    
    const req = this._newRequest("POST", 'https://api.layer.com/nonces', { 
      'Accept': 'application/vnd.layer+json; version=2.0', 
      'Content-Type': 'application/json' 
    }, (res) => {
      if (res.statusCode >= 400) {
        callback(null)
        return 
      }
      
      var data = ''
      res.setEncoding('utf8')
      res.on('data', (chunk) => {
        data += chunk
      })
      res.on('end', () => {
        callback(data)
      })
      res.on('error', (e) => {
        console.log(`ERROR: ${e}`)
      })
    })
    
    req.end()
  }
  
  _identityToken (firstName, lastName, nonce, callback) {
    const uri = URI.parse(this.appID)
    const uuid = path.basename(uri.path)
    
    const edge = `/apps/${uuid}/atlas_identities`
    
    var data = {
      nonce: nonce, 
      user: {
        first_name: firstName, 
        last_name: lastName,
        display_name: `${firstName} ${lastName}`
      }
    }
    
    const json = JSON.stringify(data)
    
    var req = this._newRequest("POST", `${IDENTITY_SERVICE_URL}${edge}`, {
      'Accept': 'application/json',
      'User-Agent': 'curl/7.44.0',
      'Content-Type': 'application/json',
      'X_LAYER_APP_ID': uuid,
      'Content-Length': json.length
    }, (res) => {
      if (res.statusCode >= 400) {
        callback(null)
        return 
      }
      
      var data = ''
      res.setEncoding('utf8')
      res.on('data', (chunk) => {
        data += chunk
      })
      res.on('end', () => {
        callback(data)
      })
    })
    req.end(json)
  }
  
  _sessionToken (identityToken, callback) {
    const data = {
      identity_token: identityToken,
      app_id: this.appID
    }
    const json = JSON.stringify(data)
    
    var req = this._newRequest("POST", "https://api.layer.com/sessions", { 
        "Accept": "application/vnd.layer+json; version=2.0",
        "Content-Type": "application/json",
        "Content-Length": json.length
      }, (res) => {
        if (res.statusCode >= 400) {
          callback(null)
          return 
        }
        
        var data = ''
        res.setEncoding('utf8')
        res.on('data', (chunk) => {
          data += chunk
        })
        res.on('end', () => {
          callback(JSON.parse(data))
        })
    })
    
    req.end(json)
  }
}

// -----------------------------------------------------------------------------------------------------------------------------------------

class TypingSimulator {
  constructor (sessionToken, conversation) {
    this.sessionToken = sessionToken
    this.conversation = conversation
  }
  
  start () {
    this._ws = new WebSocket(`wss://websockets.layer.com/?session_token=${this.sessionToken}`, 'layer-2.0')
    this._ws.on('open', () => {
      this._run(2000)
    })
  }
  
  _send (typingIndicator) {
    console.log(`${this.conversation}: Sending ${typingIndicator}`)
    const packet = {
      type: "signal",
      body: {
        type: "typing_indicator",
        object: {
          id: this.conversation
        },
        data: {
          action: typingIndicator
        }
      }
    }
    
    this._ws.send(JSON.stringify(packet))
  }
  
  _run (interval) {
    const indicators = ["started", "paused", "stopped"]
    let index = 0
    
    this._send(indicators[index++])
    
    setInterval(() => {
      this._send(indicators[index++])
      if (index % indicators.length == 0) { index = 0 }
    }, interval)
  }
}

// -----------------------------------------------------------------------------------------------------------------------------------------

const client = new LayerClient(LAYER_APP_ID)
client.authenticate(TEST_USER_FIRST_NAME, TEST_USER_LAST_NAME, (sessionToken) => {
  const sim = new TypingSimulator(sessionToken, LAYER_CONVERSATION)
  sim.start()
})
