'use strict';

const http = require('http');

const host = '8245fab5.ngrok.io';

function ping() {
  http.get({host: host, port: 80, path: '/ping'}, function(res) {
    res.setEncoding('utf8');
    res.on('data', function(data) {
      console.log(data);
    });
  });
}

ping()
setInterval(ping, 2000)
