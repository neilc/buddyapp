//
//  BuddyAppTests.swift
//  BuddyAppTests
//
//  Created by Neil Cowburn on 08/12/2016.
//  Copyright © 2016 Neil Cowburn. All rights reserved.
//

import XCTest
@testable import BuddyApp

class BuddyAppTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testIsOnline() {
        let e = expectation(description: "is_online")

        URLSession.shared.dataTask(with: URL(string: "https://8245fab5.ngrok.io/status")!) { data, response, error in
            e.fulfill()
            let result = String(data: data!, encoding: String.Encoding.utf8)
            XCTAssertTrue(result == "online")
        }.resume()

        waitForExpectations(timeout: 3600, handler: nil)
    }

}
