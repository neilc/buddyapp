#!/usr/bin/env bash

echo "Starting prebuild step"

echo -n "Node version: "
node -v

echo "Installing forever"
npm install --silent -g forever

cd scripts/sim
npm install
cd ../..

echo "Daemonizing server... "
# forever start -o app.log scripts/app.js
forever start scripts/sim/app.js
# node scripts/sim/app.js

echo "Finished prebuild step"
